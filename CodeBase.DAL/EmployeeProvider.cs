/*
*/
#region using directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using CodeBase.Entities;
using CodeBase.Entities.ViewModels;
#endregion

namespace CodeBase.DAL
{
    public class EmployeeProvider : EmployeeProviderBase, IEmployeeProvider
    {
        public EmployeeProvider() : base() { }

        public Employee Authenticate(LoginDto login)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = GetConnection();
                cmd = GetSqlCommand(conn, "dbo.usp_Employee_Authenticate", true);
                cmd.Parameters.Add(GetInParameter("@username", SqlDbType.NVarChar, login.Username));
                cmd.Parameters.Add(GetInParameter("@password", SqlDbType.NVarChar, login.Password));
                Employee tmp = null;
                IDataReader reader = ExecuteReader(cmd);
                if(reader.Read())
                {
                    var c = new Employee();
                    c.Addressid = (reader.IsDBNull(reader.GetOrdinal("Addressid"))) ? null : (Int64?)reader["Addressid"];
                    c.Code = (reader.IsDBNull(reader.GetOrdinal("Code"))) ? null : (string)reader["Code"];
                    c.Createdby = (reader.IsDBNull(reader.GetOrdinal("Createdby"))) ? null : (Int64?)reader["Createdby"];
                    c.Createddate = (reader.IsDBNull(reader.GetOrdinal("Createddate"))) ? null : (DateTime?)reader["Createddate"];
                    c.DOB = (reader.IsDBNull(reader.GetOrdinal("DOB"))) ? null : (DateTime?)reader["DOB"];
                    c.Externalemployeeid = (reader.IsDBNull(reader.GetOrdinal("Externalemployeeid"))) ? null : (string)reader["Externalemployeeid"];
                    c.Firstname = (reader.IsDBNull(reader.GetOrdinal("Firstname"))) ? null : (string)reader["Firstname"];
                    c.Genderid = (Int32)reader["Genderid"];
                    c.Gradeid = (reader.IsDBNull(reader.GetOrdinal("Gradeid"))) ? null : (Int32?)reader["Gradeid"];
                    c.ID = (Int64)reader["ID"];
                    c.Imagepath = (reader.IsDBNull(reader.GetOrdinal("Imagepath"))) ? null : (string)reader["Imagepath"];
                    c.Inductionrequired = (reader.IsDBNull(reader.GetOrdinal("Inductionrequired"))) ? null : (bool?)reader["Inductionrequired"];
                    c.Isactive = (bool)reader["Isactive"];
                    c.Ispatrolman = (reader.IsDBNull(reader.GetOrdinal("Ispatrolman"))) ? null : (bool?)reader["Ispatrolman"];
                    c.Jobtype = (reader.IsDBNull(reader.GetOrdinal("Jobtype"))) ? null : (Int32?)reader["Jobtype"];
                    c.Limitedhours = (reader.IsDBNull(reader.GetOrdinal("Limitedhours"))) ? null : (bool?)reader["Limitedhours"];
                    c.Limitedhourshours = (reader.IsDBNull(reader.GetOrdinal("Limitedhourshours"))) ? null : (decimal?)reader["Limitedhourshours"];
                    c.Maxhours = (reader.IsDBNull(reader.GetOrdinal("Maxhours"))) ? null : (Int32?)reader["Maxhours"];
                    c.Payrollinfoid = (reader.IsDBNull(reader.GetOrdinal("Payrollinfoid"))) ? null : (Int64?)reader["Payrollinfoid"];
                    c.Paytypeid = (reader.IsDBNull(reader.GetOrdinal("Paytypeid"))) ? null : (Int32?)reader["Paytypeid"];
                    c.Supplierid = (reader.IsDBNull(reader.GetOrdinal("Supplierid"))) ? null : (Int32?)reader["Supplierid"];
                    c.Surname = (reader.IsDBNull(reader.GetOrdinal("Surname"))) ? null : (string)reader["Surname"];
                    c.Typeid = (reader.IsDBNull(reader.GetOrdinal("Typeid"))) ? null : (Int32?)reader["Typeid"];
                    c.Unlicensedwork = (reader.IsDBNull(reader.GetOrdinal("Unlicensedwork"))) ? null : (bool?)reader["Unlicensedwork"];
                    c.Updatedby = (reader.IsDBNull(reader.GetOrdinal("Updatedby"))) ? null : (Int64?)reader["Updatedby"];
                    c.Updateddate = (reader.IsDBNull(reader.GetOrdinal("Updateddate"))) ? null : (DateTime?)reader["Updateddate"];
                    c.Username = (reader.IsDBNull(reader.GetOrdinal("Username"))) ? null : (string)reader["Username"];
                    c.Workrestriction = (reader.IsDBNull(reader.GetOrdinal("Workrestriction"))) ? null : (bool?)reader["Workrestriction"];
                    c.Workrestrictionhours = (reader.IsDBNull(reader.GetOrdinal("Workrestrictionhours"))) ? null : (decimal?)reader["Workrestrictionhours"];
                    c.Workrightid = (reader.IsDBNull(reader.GetOrdinal("Workrightid"))) ? null : (Int64?)reader["Workrightid"];
                    c.AcceptChanges();
                    tmp = c;
                }
                if (!reader.IsClosed) reader.Close();
                return tmp;
            }
            finally
            {
                conn?.Dispose();
                cmd?.Dispose();
            }
        }
    }
}

