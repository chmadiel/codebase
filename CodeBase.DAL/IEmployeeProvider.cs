#region using directives
using CodeBase.DAL;
using CodeBase.Entities;
using CodeBase.Entities.ViewModels;
#endregion
namespace CodeBase.DAL
{
    public interface IEmployeeProvider : IEmployeeProviderBase
    {
        Employee Authenticate(LoginDto login);
    }
}

