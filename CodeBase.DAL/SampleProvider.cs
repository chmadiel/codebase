/*
*/
#region using directives
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Collections;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using CodeBase.Entities;
#endregion

namespace CodeBase.DAL
{
    public class SampleProvider : SampleProviderBase, ISampleProvider
    {
        public SampleProvider() : base() { }

        public override Sample GetByID(long iD)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = GetConnection();
                cmd = GetSqlCommand(conn, "dbo.usp_Zone_GetByID", true);
                cmd.Parameters.Add(GetInParameter("@ID", SqlDbType.BigInt, iD));
                Sample tmp = null;
                IDataReader reader = ExecuteReader(cmd);
                if(reader.Read())
                {
                    var c = new Sample();
                    c.Createdby = (Int64)reader["Createdby"];
                    c.Createddate = (DateTime)reader["Createddate"];
                    c.ID = (Int64)reader["ID"];
                    c.Isactive = (bool)reader["Isactive"];
                    c.SampleAnotherid = (reader.IsDBNull(reader.GetOrdinal("Regionid"))) ? null : (Int32?)reader["Regionid"];
                    c.Updatedby = (reader.IsDBNull(reader.GetOrdinal("Updatedby"))) ? null : (Int64?)reader["Updatedby"];
                    c.Updateddate = (reader.IsDBNull(reader.GetOrdinal("Updateddate"))) ? null : (DateTime?)reader["Updateddate"];
                    c.Samplename = (reader.IsDBNull(reader.GetOrdinal("Zonename"))) ? null : (string)reader["Zonename"];
                    c.SampleAnotherName = (string)reader["RegionName"];
                    c.AcceptChanges();
                    tmp = c;
                }
                if (!reader.IsClosed) reader.Close();
                return tmp;
            }
            finally
            {
                conn?.Dispose();
                cmd?.Dispose();
            }
        }

        public override Sample GetActiveByID(long iD)
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = GetConnection();
                cmd = GetSqlCommand(conn, "dbo.usp_Zone_GetActiveByID", true);
                cmd.Parameters.Add(GetInParameter("@ID", SqlDbType.BigInt, iD));
                Sample tmp = null;
                IDataReader reader = ExecuteReader(cmd);
                if (reader.Read())
                {
                    var c = new Sample();
                    c.Createdby = (Int64)reader["Createdby"];
                    c.Createddate = (DateTime)reader["Createddate"];
                    c.ID = (Int64)reader["ID"];
                    c.Isactive = (bool)reader["Isactive"];
                    c.SampleAnotherid = (reader.IsDBNull(reader.GetOrdinal("Regionid"))) ? null : (Int32?)reader["Regionid"];
                    c.Updatedby = (reader.IsDBNull(reader.GetOrdinal("Updatedby"))) ? null : (Int64?)reader["Updatedby"];
                    c.Updateddate = (reader.IsDBNull(reader.GetOrdinal("Updateddate"))) ? null : (DateTime?)reader["Updateddate"];
                    c.Samplename = (reader.IsDBNull(reader.GetOrdinal("Zonename"))) ? null : (string)reader["Zonename"];
                    c.SampleAnotherName = (string)reader["RegionName"];
                    c.AcceptChanges();
                    tmp = c;
                }
                if (!reader.IsClosed) reader.Close();
                return tmp;
            }
            finally
            {
                conn?.Dispose();
                cmd?.Dispose();
            }
        }

        public override IList<Sample> GetAll()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = GetConnection();
                cmd = GetSqlCommand(conn, "dbo.usp_Zone_GetAll", true);
                IList<Sample> tmp = new List<Sample>();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    var c = new Sample();
                    c.Createdby = (Int64)reader["Createdby"];
                    c.Createddate = (DateTime)reader["Createddate"];
                    c.ID = (Int64)reader["ID"];
                    c.Isactive = (bool)reader["Isactive"];
                    c.SampleAnotherid = (reader.IsDBNull(reader.GetOrdinal("Regionid"))) ? null : (Int32?)reader["Regionid"];
                    c.Updatedby = (reader.IsDBNull(reader.GetOrdinal("Updatedby"))) ? null : (Int64?)reader["Updatedby"];
                    c.Updateddate = (reader.IsDBNull(reader.GetOrdinal("Updateddate"))) ? null : (DateTime?)reader["Updateddate"];
                    c.Samplename = (reader.IsDBNull(reader.GetOrdinal("Zonename"))) ? null : (string)reader["Zonename"];
                    c.SampleAnotherName = (string)reader["RegionName"];
                    c.AcceptChanges();
                    tmp.Add(c);
                }
                if (!reader.IsClosed) reader.Close();
                return tmp;
            }
            finally
            {
                conn?.Dispose();
                cmd?.Dispose();
            }
        }

        public override IList<Sample> GetAllActive()
        {
            SqlConnection conn = null;
            SqlCommand cmd = null;
            try
            {
                conn = GetConnection();
                cmd = GetSqlCommand(conn, "dbo.usp_Zone_GetAllActive", true);
                IList<Sample> tmp = new List<Sample>();
                IDataReader reader = ExecuteReader(cmd);
                while (reader.Read())
                {
                    var c = new Sample();
                    c.Createdby = (Int64)reader["Createdby"];
                    c.Createddate = (DateTime)reader["Createddate"];
                    c.ID = (Int64)reader["ID"];
                    c.Isactive = (bool)reader["Isactive"];
                    c.SampleAnotherid = (reader.IsDBNull(reader.GetOrdinal("Regionid"))) ? null : (Int32?)reader["Regionid"];
                    c.Updatedby = (reader.IsDBNull(reader.GetOrdinal("Updatedby"))) ? null : (Int64?)reader["Updatedby"];
                    c.Updateddate = (reader.IsDBNull(reader.GetOrdinal("Updateddate"))) ? null : (DateTime?)reader["Updateddate"];
                    c.Samplename = (reader.IsDBNull(reader.GetOrdinal("Zonename"))) ? null : (string)reader["Zonename"];
                    c.SampleAnotherName = (string)reader["RegionName"];
                    c.AcceptChanges();
                    tmp.Add(c);
                }
                if (!reader.IsClosed) reader.Close();
                return tmp;
            }
            finally
            {
                conn?.Dispose();
                cmd?.Dispose();
            }
        }
    }
}

