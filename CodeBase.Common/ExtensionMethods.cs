﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeBase.Common
{
    public static class ExtensionMethods
    {
        public static string ReplaceSingleQuotesWithDouble(this string theValue)
        {
            return theValue.Replace("'", @"'''");
        }

        public static bool IsNumeric(this string theValue)
        {
            return long.TryParse(theValue, NumberStyles.Integer, NumberFormatInfo.InvariantInfo, out _);
        }

        public static bool IsDate(this string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                return DateTime.TryParse(input, out _);
            }
            return false;
        }

        public static DateTime ToDate(this string input, bool throwExceptionIfFailed = false)
        {
            var valid = DateTime.TryParse(input, out DateTime result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException($"'{input}' cannot be converted as DateTime");
            return result;
        }

        public static int ToInt(this string input, bool throwExceptionIfFailed = false)
        {
            var valid = int.TryParse(input, out int result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException($"'{input}' cannot be converted as int");
            return result;
        }

        public static double ToDouble(this string input, bool throwExceptionIfFailed = false)
        {
            var valid = double.TryParse(input, NumberStyles.AllowDecimalPoint,
                new NumberFormatInfo { NumberDecimalSeparator = "." }, out double result);
            if (!valid)
                if (throwExceptionIfFailed)
                    throw new FormatException($"'{input}' cannot be converted as double");
            return result;
        }

        public static string Reverse(this string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return string.Empty;
            var chars = input.ToCharArray();
            Array.Reverse(chars);
            return new string(chars);
        }

        /// <summary>
        ///     Matching all capital letters in the input and seperate them with spaces to form a sentence.
        ///     If the input is an abbreviation text, no space will be added and returns the same input.
        /// </summary>
        /// <example>
        ///     input : HelloWorld
        ///     output : Hello World
        /// </example>
        /// <example>
        ///     input : BBC
        ///     output : BBC
        /// </example>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToSentence(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return input;
            //return as is if the input is just an abbreviation
            if (Regex.Match(input, "[0-9A-Z]+$").Success)
                return input;
            //add a space before each capital letter, but not the first one.
            var result = Regex.Replace(input, "(\\B[A-Z])", " $1");
            return result;
        }

        public static string GetLast(this string input, int howMany)
        {
            if (string.IsNullOrWhiteSpace(input)) return string.Empty;
            var value = input.Trim();
            return howMany >= value.Length ? value : value.Substring(value.Length - howMany);
        }

        public static string GetFirst(this string input, int howMany)
        {
            if (string.IsNullOrWhiteSpace(input)) return string.Empty;
            var value = input.Trim();
            return howMany >= value.Length ? value : input.Substring(0, howMany);
        }

        public static bool IsEmail(this string input)
        {
            var match = Regex.Match(input, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
            return match.Success;
        }

        public static bool IsPhone(this string input)
        {
            var match = Regex.Match(input, @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$", RegexOptions.IgnoreCase);
            return match.Success;
        }

        public static bool IsNumber(this string input)
        {
            var match = Regex.Match(input, @"^[0-9]+$", RegexOptions.IgnoreCase);
            return match.Success;
        }

        public static int ExtractNumber(this string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return 0;

            var match = Regex.Match(input, "[0-9]+", RegexOptions.IgnoreCase);
            return match.Success ? match.Value.ToInt() : 0;
        }

        public static string ExtractEmail(this string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return string.Empty;

            var match = Regex.Match(input, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
            return match.Success ? match.Value : string.Empty;
        }

        public static string ExtractQueryStringParamValue(this string queryString, string paramName)
        {
            if (string.IsNullOrWhiteSpace(queryString) || string.IsNullOrWhiteSpace(paramName)) return string.Empty;

            var query = queryString.Replace("?", "");
            if (!query.Contains("=")) return string.Empty;
            var queryValues =
                query.Split('&')
                    .Select(piQ => piQ.Split('='))
                    .ToDictionary(piKey => piKey[0].ToLower().Trim(), piValue => piValue[1]);

            var found = queryValues.TryGetValue(paramName.ToLower().Trim(), out string result);
            return found ? result : string.Empty;
        }

        public static DateTime StartOfDay(this DateTime theDate)
        {
            return theDate.Date;
        }

        public static DateTime EndOfDay(this DateTime theDate)
        {
            return theDate.Date.AddDays(1).AddTicks(-1);
        }

        public static DateTime FirstDayOfMonth(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, 1);
        }

        public static DateTime LastDayOfMonth(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, DateTime.DaysInMonth(value.Year, value.Month));
        }

        public static bool CheckForSqlInjection(string userInput)
        {
            var isSqlInjection = false;
            string[] sqlCheckList =
            {
                "--",
                ";--",
                ";",
                "/*",
                "*/",
                "@@",
                "@",
                "char",
                "nchar",
                "varchar",
                "nvarchar",
                "alter",
                "begin",
                "cast",
                "create",
                "cursor",
                "declare",
                "delete",
                "drop",
                "end",
                "exec",
                "execute",
                "fetch",
                "insert",
                "kill",
                "select",
                "sys",
                "sysobjects",
                "syscolumns",
                "table",
                "update"
            };
            var checkString = userInput.Replace("'", "''");
            for (var i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if (checkString.IndexOf(sqlCheckList[i],
                    StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    isSqlInjection = true;
                }
            }
            return isSqlInjection;
        }
    }
}
