﻿using System;

namespace CodeBase.Common
{
    [Serializable]
    public class AppException : System.ApplicationException
    {

        private readonly string _title = "Lrmis Application Exception";
        private readonly string _userMessage;

        public string Title => string.IsNullOrEmpty(_title) ? "Lrmis Application Error" : _title;
        public string UserMessage => string.IsNullOrEmpty(_userMessage) ? Message : _userMessage;

        /// <summary>
        /// 
        /// </summary>
        public AppException()
            : base("Application Error")
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message and UserMessage will be the same</param>
        public AppException(string message)
            : base(message)
        {
            _userMessage = message;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message and UserMessage will be the same</param>
        /// <param name="title">Title to be shown to the user</param>
        public AppException(string message, string title)
            : base(message)
        {
            _userMessage = message;
            _title = title;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="title"></param>
        /// <param name="userMessage">Message to be shown to the user, other than Exception message</param>
        public AppException(string message, string title, string userMessage)
            : base(message)
        {
            _title = title;
            _userMessage = userMessage;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message">Message and UserMessage will be same here</param>
        /// <param name="innerException"></param>
        public AppException(string message, Exception innerException)
            : base(message, innerException)
        {
            _userMessage = message;
        }

    }

    [Serializable]
    public class DataValidationException : AppException
    {

        public DataValidationException() : base("Data Validation Error(s)")
        {

        }

    }

    [Serializable]
    public class DataDuplicationException : AppException
    {
        public String DuplicateFieldName { get; set; }
        public DataDuplicationException(string message, string fieldName)
            : base(message + " DuplicateFieldName:" + fieldName)
        {
            DuplicateFieldName = fieldName;
        }

    }

    /// <summary>
    ///     Exception class for Fraction, derived from CustomException
    /// </summary>
    /// 
    [Serializable]
    public class FractionException : AppException
    {

        public FractionException(string message)
            : base(message)
        {
        }

        public FractionException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
