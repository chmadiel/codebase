﻿using System.Collections.Generic;

namespace CodeBase.Common
{
    public static class CacheConstants
    {
        public const string PayTypeCacheKey = "PAYTYPE_CACHE_KEY";
        public const string GradeCacheKey = "GRADE_CACHE_KEY";
        public const string PayTypeGradeCacheKey = "PAYTYPE_GRADE_CACHE_KEY";
        public const string AccountTypeCacheKey = "ACCOUNTTYPE_CACHE_KEY";
        public const string LicenseCacheKey = "LICENSE_CACHE_KEY";
        public const string SupplierCacheKey = "SUPPLIER_CACHE_KEY";
        public const string ResidencyStatusCacheKey = "RESIDENCYSTATUS_CACHE_KEY";

        public const string RegionCacheKey = "REGION_CACHE_KEY";
        public const string ZoneCacheKey = "ZONE_CACHE_KEY";
        public const string SiteCacheKey = "SITE_CACHE_KEY";
        public const string RegionZoneCacheKey = "REGION_ZONE_CACHE_KEY";
        public const string ZoneSiteCacheKey = "ZONE_SITE_CACHE_KEY";
        public const string CountryCacheKey = "COUNTRY_CACHE_KEY";
        public const string StateCacheKey = "STATE_CACHE_KEY";
        public const string TimeZoneCacheKey = "TIMEZONE_CACHE_KEY";
        public const string ContractCacheKey = "CONTRACT_CACHE_KEY";
        public const string SiteContractCacheKey = "SITE_CONTRACT_CACHE_KEY";
        public const string BillItemCacheKey = "BILLITEM_CACHE_KEY";

        public const string FieldTypeCacheKey = "FIELD_TYPE_CACHE_KEY";
        public const string FieldCacheKey = "FIELD_CACHE_KEY";
        public const string ItemCacheKey = "ITEM_CACHE_KEY";

        public const string UniformFieldCacheKey = "UNIFORM_FIELD_CACHE_KEY";
        public const string UniformItemCacheKey = "UNIFORM_ITEM_CACHE_KEY";
        public const string UniformFieldUniformCacheKey = "UNIFORM_FIELD_UNIFORM_CACHE_KEY";
        public const string UniformItemUniformCacheKey = "UNIFORM_ITEM_UNIFORM_CACHE_KEY";
    }

    public static class Constants
    {
        public static List<DayOfWeek> WeekDays => new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday, DayOfWeek.Saturday, DayOfWeek.Sunday };
    }

    public enum DayOfWeek
    {
        Monday = 1,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday = 7
    }

    public enum AvailabilityType
    {
        Available = 1,
        Maybe,
        NotAvailable = 3
    }

    public enum JobType
    {
        FullTime = 1,
        Casual = 2
    }

    public enum FileType
    {
        Image = 1,
        Video = 2
    }

    public enum FolderName
    {
        Content = 1,
        Images,
        Videos
    }

    public enum SiteType
    {
        Client = 1,
        Multi,
        Site
    }

    public enum LicenseRequirement
    {
        Hard = 1,
        Conditional,
        Soft
    }

    public enum UnitType
    {
        PerItem = 1,
        PerHour
    }

    public enum ServiceType
    {
        HourlyGaurding = 1,
        ScheduledPatrolsEnforce,
        ScheduledPatrolsOngoing,
        RecurrentFixedCost,
        DispatchService
    }

    public enum PaymentMethod
    {
        CreditCard = 1,
        Cheque,
        Cash,
        BankTransfer
    }

    public enum PaymentTerms
    {
        DueUponReceipt = 1,
        Net7,
        Net10,
        Net14,
        Net15,
        Net20,
        Net30,
        Net40,
        Net45,
        Net60,
        Net90,
        Net120
    }

    public enum IncludeTimesheets
    {
        DoNotInclude = 1,
        IncludeEveryShift,
        GroupedByDatePositionRate,
        GroupedByPositionRate,
        GroupedByEmployeeRate,
        GroupedByDatePositionSubtotals
    }

    public enum BillingRecurrence
    {
        Weekly = 1,
        Every10Days,
        BiWeekly,
        SemiMonthly,
        Monthly,
        Quarterly,
        Yearly,
        AdhocManually
    }

    public enum InvoiceDateAdjustment
    {
        Remove1Day = 1,
        NoAdjustment,
        Add1Day,
        Add2Days,
        Add3Days,
        Add4Days,
        Add5Days,
        Add6Days,
        Add7Days,
        Add8Days,
        Add9Days,
        Add10Days,
        Add11Days,
        Add12Days,
        Add13Days,
        Add14Days,
        Add15Days,
        Add16Days,
        Add17Days,
        Add18Days,
        Add19Days,
        Add20Days,
        Add21Days,
        Add22Days,
        Add23Days,
        Add24Days,
        Add25Days,
        Add26Days,
        Add27Days,
        Add28Days,
        Add29Days
    }

    public enum FieldTypeEnum
    {
        Checkbox = 1,
        Textbox
    }
}
