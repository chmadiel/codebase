﻿using System;

namespace CodeBase.Common
{
    public class ConfigurationReader
    {
        private static AppSettings _settings;

        public static void SetSettings(AppSettings settings)
        {
            _settings = settings;
        }

        public static AppSettings GetSettings()
        {
            return _settings;
        }

        public static int GetDefaultCommandTimeOutDuration()
        {
            return Convert.ToInt32(_settings.DefautTimeout ?? "60");
        }

        public static string GetCryproIV()
        {
            return Convert.ToString(_settings.CryptoIV ?? @"!QAZ2WSX#EDC4RFV");
        }

        public static string GetCryproKey()
        {
            return Convert.ToString(_settings.CryptoKey ?? @"5TGB&YHN7UJM(IK<5TGB&YHN7UJM(IK<");
        }
        public static int GetLookupCacheDuration()
        {
            //LookupCacheDurationSeconds
            return Convert.ToInt32(_settings.DefautTimeout ?? "3600");
        }
        public static int GetTerritoryLookupCacheDuration()
        {
            //TerritoryDataCacheDurationSeconds
            return Convert.ToInt32(_settings.DefautTimeout ?? "7200");
        }
        public static int GetDataCacheDuration()
        {
            //DataCacheDurationSeconds
            return Convert.ToInt32(_settings.DefautTimeout ?? "900");
        }
    }
}
