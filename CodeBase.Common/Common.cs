﻿using Microsoft.AspNetCore.Hosting;
using System;
using System.Drawing;
using System.IO;

namespace CodeBase.Common
{
    public class Common
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public Common(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public string UploadFile(string base64File, string fileName = "x.jpg", FolderName folderName = FolderName.Images, FileType fileType = FileType.Image)
        {
            var bytes = Convert.FromBase64String(base64File.Split(',')[1]);
            var uniqueFileName = Guid.NewGuid().ToString() + Path.GetExtension(fileName);
            var savePath = Path.Combine(_hostingEnvironment.ContentRootPath, FolderName.Content.ToString(), folderName.ToString(), uniqueFileName);

            if (fileType == FileType.Image)
            {
                using Image image = Image.FromStream(new MemoryStream(bytes));
                image.Save(savePath);
            }

            return uniqueFileName;
        }
    }
}
