﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBase.Common
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string DefautTimeout { get; set; }
        public string Secret { get; set; }
        public string CryptoIV { get; set; }
        public string CryptoKey { get; set; }
    }
}
