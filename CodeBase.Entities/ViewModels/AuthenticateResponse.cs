﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBase.Entities.ViewModels
{
    [Serializable]
    public class AuthenticateResponse
    {
        public Employee Employee { get; set; }
        public string Token { get; set; }

        public AuthenticateResponse(Employee employee, string token)
        {
            Employee = employee;
            Token = token;
        }
    }
}
