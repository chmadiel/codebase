﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CodeBase.Entities.ViewModels
{
    [Serializable]
    public class LoginDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
