﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeBase.Entities.ViewModels
{
    public class ValidationMessage
    {
        public bool IsValid { get; set; }
        public List<string> ErrorMessages { get; set; }
    }
}
