﻿namespace CodeBase.Entities.ViewModels
{
    public class ApiResponse
    {
        public bool OperationStatus { get; set; }

        private object _additionalData { get; set; }

        public object AdditionalData
        {
            get
            {
                if (_additionalData == null)
                    return new string[0];
                else
                    return _additionalData;
            }
            set { _additionalData = value; }
        }

        public byte MessageType { get; set; }
        public string Message { get; set; }
    }
}
