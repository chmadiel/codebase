﻿namespace CodeBase.Entities.CacheObjects
{
    public class SampleCache
    {
        public long SampleId { get; set; }
        public string SampleName { get; set; }
        public int SampleAnotherId { get; set; }

        public SampleCache(long zoneId, string zoneName, int sampleAnotherId)
        {
            SampleId = zoneId;
            SampleName = zoneName;
            SampleAnotherId = sampleAnotherId;
        }
    }
}
