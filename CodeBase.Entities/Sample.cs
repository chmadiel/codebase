/*
File generated by custome templates
Important: Do not modify this file. Edit the fileZone.cs instead.
*/
#region using directives
using System;
#endregion

namespace CodeBase.Entities
{
    [Serializable]
    public class Sample : SampleBase
    {
        #region Constructors
        public Sample() : base() { }
        #endregion
        public string SampleAnotherName { get; set; }
    }
}

