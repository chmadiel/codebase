/*
File generated by custome templates
Important: Do not modify this file. Edit the fileEmployee.cs instead.
*/
#region using directives
using System;
using System.Collections.Generic;
#endregion

namespace CodeBase.Entities
{
    [Serializable]
    public class Employee : EmployeeBase
    {
        #region Constructors
        public Employee() : base()
        {
            Createddate = DateTime.UtcNow;
        }
        #endregion

        public string Base64Image { get; set; }
        public string ImageName { get; set; }

        public string DobName
        {
            get
            {
                return base.DOB.ToString();
            }
            set
            {
                base.DOB = DateTime.Parse(value);
            }
        }
        public string PaytypeName { get; set; }
        public string SeviceAreaName { get; set; }
        public string SupplierName { get; set; }
    }
}

