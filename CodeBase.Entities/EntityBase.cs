﻿using System;

namespace CodeBase.Entities
{
    [Serializable]
    public class EntityBase
    {
        #region Constructors

        public EntityBase()
        {
            RowState = EntityState.New;
        }

        #endregion

        public EntityState RowState { get; set; }

        public void AcceptChanges()
        {
            RowState = EntityState.Unchanged;
        }

        public void RejectChanges()
        {
            RowState = EntityState.Unchanged;
        }

        public void EntityModified()
        {
            RowState = EntityState.Modified;
        }

        public void EntityDeleted()
        {
            RowState = EntityState.Deleted;
        }

        public void EntityCreated()
        {
            RowState = EntityState.New;
        }

        public bool IsValid()
        {
            return true;
        }
    }

    public enum EntityState
    {
        Unchanged = 0,
        New = 1,
        Modified = 2,
        Deleted = 3,
        Excluded = 4
    }
}
