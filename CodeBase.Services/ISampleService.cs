/*
*/
#region using directives
using CodeBase.Services;
#endregion

namespace CodeBase.Services
{
    public interface ISampleService : ISampleServiceBase { }
}
