﻿using CodeBase.Entities;
using CodeBase.Entities.CacheObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBase.Services
{
    public interface IDataCache
    {
        IList<SampleCache> GetAllZones();
        void AddCacheItem(string cacheItemKey, object cacheItem);
        object GetCacheItem(string cacheItemKey);
        void AddSerializedCacheItem(string cacheItemKey, object serializableCacheItem);
        object GetSerializedCacheItem(string cacheItemKey);
        bool ContainsSerializedCacheItem(string cacheItemKey);
        void RemoveRegionZone();
        void RemoveZoneSite();
        void RefreshCache();
    }
}
