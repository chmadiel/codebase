﻿using CodeBase.Common;
using CodeBase.Entities;
using CodeBase.Entities.CacheObjects;
using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace CodeBase.Services
{
    public class DataCache : IDataCache
    {
        #region private constants and variables

        internal static readonly ObjectCache Cache = MemoryCache.Default;
        private readonly CacheEntryRemovedCallback _callback = null;
        IDataCacheHelper _dataCacheHelper;

        private static readonly int LookupCacheDuration = ConfigurationReader.GetLookupCacheDuration();
        private static readonly int TerritoryCacheDuration = ConfigurationReader.GetTerritoryLookupCacheDuration();
        private static readonly int CacheDuration = ConfigurationReader.GetDataCacheDuration();

        #endregion

        #region constructors and Getinstance method

        public DataCache(IDataCacheHelper dataCacheHelper)
        {
            _dataCacheHelper = dataCacheHelper;
            Task.Run(() => CacheData());
        }

        private async Task CacheData()
        {
            await Task.Run(() => _dataCacheHelper.CacheTerritoryData(this));
        }

        #endregion

        #region Get Methods of different Cache Items

        #region Sample Get Methods

        public IList<SampleCache> GetAllZones()
        {
            return _dataCacheHelper.GetAllZones(this, 0);
        }

        /// <summary>
        ///     returns list of Districts for a division, if not found then returns empty list
        /// </summary>
        /// <param name="regionId">list filtered by this param</param>
        /// <returns></returns>
        public IList<SampleCache> GetZonesListByRegionId(int regionId)
        {
            return GetZonesListByRegionId(this, regionId, 0);
        }

        private IList<SampleCache> GetZonesListByRegionId(DataCache dataCache, int regionId, int callNo)
        {
            var cacheItem =
                Cache.Get(CacheConstants.RegionZoneCacheKey) as Dictionary<int, IList<SampleCache>>;

            if (callNo > 1)
                return cacheItem != null && cacheItem.ContainsKey(regionId)
                    ? cacheItem[regionId]
                    : new List<SampleCache>();


            if (cacheItem == null)
            {
                _dataCacheHelper.CacheTerritoryData(dataCache);
                callNo++;
                return GetZonesListByRegionId(dataCache, regionId, callNo);
            }

            return cacheItem.ContainsKey(regionId) ? cacheItem[regionId] : new List<SampleCache>();
        }

        #endregion

        #region Load Policies

        /// <summary>
        ///     creates cache property
        /// </summary>
        /// <returns></returns>
        internal CacheItemPolicy GetLookupPolicy()
        {
            return new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default,
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(LookupCacheDuration),
                RemovedCallback = _callback
            };
        }

        internal CacheItemPolicy GetTerritoryPolicy()
        {
            return new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default,
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(TerritoryCacheDuration),
                RemovedCallback = _callback
            };
        }

        internal CacheItemPolicy GetDataItemPolicy()
        {
            return new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default,
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(CacheDuration),
                RemovedCallback = _callback
            };
        }

        private CacheItemPolicy GetLocalDataItemPolicy()
        {
            return new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default,
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(CacheDuration),
                RemovedCallback = _callback
            };
        }

        internal CacheItemPolicy GetUniformPolicy()
        {
            return new CacheItemPolicy
            {
                Priority = CacheItemPriority.Default,
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(TerritoryCacheDuration),
                RemovedCallback = _callback
            };
        }

        #endregion

        #endregion

        #region Add custom cache object

        /// <summary>
        ///     finds key, if exist then replace existing item. otherwise add its to the cache
        /// </summary>
        public void AddCacheItem(string cacheItemKey, object cacheItem)
        {
            var policy = GetDataItemPolicy();
            Cache.Set(cacheItemKey, cacheItem, policy);
        }

        public object GetCacheItem(string cacheItemKey)
        {
            return Cache.Contains(cacheItemKey) ? Cache.Get(cacheItemKey) : null;
        }


        /// <summary>
        ///     use this cache method to store large blobs as serialized objects. these serialized cache will be removed
        ///     if not used intime.
        /// </summary>
        /// <param name="cacheItemKey"></param>
        /// <param name="serializableCacheItem">item has to be serializable.</param>
        public void AddSerializedCacheItem(string cacheItemKey, object serializableCacheItem)
        {
            var policy = GetLocalDataItemPolicy();
            Cache.Set(cacheItemKey, serializableCacheItem, policy);
        }

        public object GetSerializedCacheItem(string cacheItemKey)
        {
            return Cache.Contains(cacheItemKey) ? Cache.Get(cacheItemKey) : null;
        }

        public bool ContainsSerializedCacheItem(string cacheItemKey)
        {
            return Cache.Contains(cacheItemKey);
        }

        #endregion

        #region Misc Get Methods

        //public RolePermissions GetRolePermissions()
        //{
        //    return DataCacheHelper.GetRolePermissions(this, 0);
        //}

        #endregion

        #region Load Data for different caching items

        #endregion

        #region Sample Add Methods
        ///<summary>
        ///   Remove required data to refresh again
        ///         
        ///</summary> 
        public void RemoveRegionZone()
        {
            Cache.Remove(CacheConstants.RegionZoneCacheKey);
        }

        ///<summary>
        ///   Remove required data to refresh again
        ///         
        ///</summary> 
        public void RemoveZoneSite()
        {
            Cache.Remove(CacheConstants.ZoneSiteCacheKey);
        }

        public void RefreshCache()
        {
            _dataCacheHelper.CacheTerritoryData(this);
        }

        public void AddZoneToCache(Sample zone)
        {
            _dataCacheHelper.AddZoneToCache(this, zone);
        }

        #endregion
    }
}
