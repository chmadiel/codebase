/*
*/
#region using directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using CodeBase.DAL;
using CodeBase.Entities;
using System.Transactions;
#endregion

namespace CodeBase.Services
{
    public class SampleServiceBase : ServiceBase
    {
        protected ISampleProvider provider;
        public SampleServiceBase(ISampleProvider provider)
        {
            this.provider = provider;
        }
        public virtual Sample Save(Sample entity, bool createTransaction)
        {
            TransactionScope scope = null;
            try
            {
                if (createTransaction)
                {
                    scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted });
                }
                Save(entity);
                scope?.Complete();
            }
            finally
            {
                scope?.Dispose();
            }
            return entity;
        }
        [DataObjectMethod(DataObjectMethodType.Update)]
        internal virtual Sample Save(Sample entity)
        {
            if (!entity.IsValid())
            {
            }
            switch (entity.RowState)
            {
                case EntityState.New:
                    provider.Insert(entity);
                    break;
                case EntityState.Modified:
                    provider.Update(entity);
                    break;
                case EntityState.Deleted:
                    provider.Delete(entity);
                    break;
            }
            return entity;
        }
        public virtual IList<Sample> Save(IList<Sample> entityCollection, bool createTransaction)
        {
            TransactionScope scope = null;
            try
            {
                if (createTransaction)
                {
                    scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted });
                }
                Save(entityCollection);
                scope?.Complete();
            }
            finally
            {
                scope?.Dispose();
            }
            return entityCollection;
        }
        [DataObjectMethod(DataObjectMethodType.Update)]
        internal virtual IList<Sample> Save(IList<Sample> entityCollection)
        {
            bool isAllValid = true;
            try
            {
                foreach (var entity in entityCollection)
                {
                    if (!entity.IsValid()) isAllValid = false;
                }
                if (!isAllValid)
                {
                }

                provider.Update(entityCollection);
                return entityCollection;
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual Sample GetByID(Int64 iD)
        {
            try
            {

                var entity = provider.GetByID(iD);
                if (entity == null) return new Sample();
                return entity;
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Sample> GetListByForeignKeyRegionid(Int32? regionid)
        {
            try
            {

                return provider.GetListByForeignKeyRegionid(regionid);
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Sample> GetAllActive()
        {
            try
            {

                return provider.GetAllActive();
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual Sample GetActiveByID(Int64 iD)
        {
            try
            {

                var entity = provider.GetActiveByID(iD);
                if (entity == null) return new Sample();
                return entity;
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Sample> GetActiveListByForeignKeyRegionid(Int32? regionid)
        {
            try
            {
                var provider = new SampleProvider();
                return provider.GetActiveListByForeignKeyRegionid(regionid);
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Sample> GetAll()
        {
            try
            {

                var collection = provider.GetAll();
                return collection;
            }
            finally
            {
            }
        }
    }
}

