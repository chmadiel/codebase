/*
*/
#region using directives
using CodeBase.Entities;
using System;
using System.Collections.Generic;
#endregion

namespace CodeBase.Services
{
    public interface IEmployeeServiceBase
    {
        Employee Save(Employee entity, bool createTransaction);
        IList<Employee> Save(IList<Employee> entityCollection, bool createTransaction);
        Employee GetByID(Int64 iD);
        IList<Employee> GetListByForeignKeyPayrollinfoid(Int64? payrollinfoid);
        IList<Employee> GetListByForeignKeyWorkrightid(Int64? workrightid);
        IList<Employee> GetAllActive();
        Employee GetActiveByID(Int64 iD);
        IList<Employee> GetActiveListByForeignKeyPayrollinfoid(Int64? payrollinfoid);
        IList<Employee> GetActiveListByForeignKeyWorkrightid(Int64? workrightid);
        IList<Employee> GetAll();
    }
}

