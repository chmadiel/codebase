/*
*/
#region using directives
using CodeBase.DAL;
using CodeBase.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Transactions;
#endregion

namespace CodeBase.Services
{
    public class EmployeeServiceBase : ServiceBase
    {
        protected IEmployeeProvider provider;
        public EmployeeServiceBase(IEmployeeProvider provider)
        {
            this.provider = provider;
        }
        public virtual Employee Save(Employee entity, bool createTransaction)
        {
            TransactionScope scope = null;
            try
            {
                if (createTransaction)
                {
                    scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted });
                }
                Save(entity);
                scope?.Complete();
            }
            finally
            {
                scope?.Dispose();
            }
            return entity;
        }
        [DataObjectMethod(DataObjectMethodType.Update)]
        internal virtual Employee Save(Employee entity)
        {
            if (!entity.IsValid())
            {
            }
            switch (entity.RowState)
            {
                case EntityState.New:
                    provider.Insert(entity);
                    break;
                case EntityState.Modified:
                    provider.Update(entity);
                    break;
                case EntityState.Deleted:
                    provider.Delete(entity);
                    break;
            }
            return entity;
        }
        public virtual IList<Employee> Save(IList<Employee> entityCollection, bool createTransaction)
        {
            TransactionScope scope = null;
            try
            {
                if (createTransaction)
                {
                    scope = new TransactionScope(TransactionScopeOption.Required,
                    new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted });
                }
                Save(entityCollection);
                scope?.Complete();
            }
            finally
            {
                scope?.Dispose();
            }
            return entityCollection;
        }
        [DataObjectMethod(DataObjectMethodType.Update)]
        internal virtual IList<Employee> Save(IList<Employee> entityCollection)
        {
            bool isAllValid = true;
            try
            {
                foreach (var entity in entityCollection)
                {
                    if (!entity.IsValid()) isAllValid = false;
                }
                if (!isAllValid)
                {
                }

                provider.Update(entityCollection);
                return entityCollection;
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual Employee GetByID(Int64 iD)
        {
            try
            {

                var entity = provider.GetByID(iD);
                if (entity == null) return new Employee();
                return entity;
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Employee> GetListByForeignKeyPayrollinfoid(Int64? payrollinfoid)
        {
            try
            {

                return provider.GetListByForeignKeyPayrollinfoid(payrollinfoid);
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Employee> GetListByForeignKeyWorkrightid(Int64? workrightid)
        {
            try
            {

                return provider.GetListByForeignKeyWorkrightid(workrightid);
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Employee> GetAllActive()
        {
            try
            {

                return provider.GetAllActive();
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual Employee GetActiveByID(Int64 iD)
        {
            try
            {

                var entity = provider.GetActiveByID(iD);
                if (entity == null) return new Employee();
                return entity;
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Employee> GetActiveListByForeignKeyPayrollinfoid(Int64? payrollinfoid)
        {
            try
            {
                var provider = new EmployeeProvider();
                return provider.GetActiveListByForeignKeyPayrollinfoid(payrollinfoid);
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Employee> GetActiveListByForeignKeyWorkrightid(Int64? workrightid)
        {
            try
            {
                var provider = new EmployeeProvider();
                return provider.GetActiveListByForeignKeyWorkrightid(workrightid);
            }
            finally
            {
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select)]
        public virtual IList<Employee> GetAll()
        {
            try
            {

                var collection = provider.GetAll();
                return collection;
            }
            finally
            {
            }
        }
    }
}

