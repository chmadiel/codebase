﻿using CodeBase.Entities;
using CodeBase.Entities.CacheObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeBase.Services
{
    public interface IDataCacheHelper
    {
        IList<SampleCache> GetAllZones(DataCache dataCache, int callNo);
        void CacheTerritoryData(DataCache dataCache);
        void AddZoneToCache(DataCache dataCache, Sample zone);
    }
}
