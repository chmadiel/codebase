/*
*/
#region using directives
using System;
using System.Collections.Generic;
using System.ComponentModel;
using CodeBase.Entities;
#endregion

namespace CodeBase.Services
{
    public interface ISampleServiceBase
    {
        Sample Save(Sample entity, bool createTransaction);
        IList<Sample> Save(IList<Sample> entityCollection, bool createTransaction);
        Sample GetByID(Int64 iD);
        IList<Sample> GetListByForeignKeyRegionid(Int32? regionid);
        IList<Sample> GetAllActive();
        Sample GetActiveByID(Int64 iD);
        IList<Sample> GetActiveListByForeignKeyRegionid(Int32? regionid);
        IList<Sample> GetAll();
    }
}

