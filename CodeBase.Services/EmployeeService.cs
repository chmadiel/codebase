/*
*/
#region using directives
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using CodeBase.Common;
using CodeBase.DAL;
using CodeBase.Entities;
using CodeBase.Entities.ViewModels;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Transactions;
#endregion

namespace CodeBase.Services
{
    public class EmployeeService : EmployeeServiceBase, IEmployeeService
    {
        #region Properties
        
        readonly AppSettings _appSettings;
        readonly IDataCache _dataCache;
        readonly Common.Common _common;

        #endregion

        #region Constructors

        public EmployeeService(IEmployeeProvider provider, IDataCache dataCache, Common.Common common) : base(provider)
        {
            _appSettings = ConfigurationReader.GetSettings();

            _dataCache = dataCache;

            _common = common;
        }

        #endregion

        #region Authentication

        public AuthenticateResponse Authenticate(LoginDto login)
        {
            login.Password = CryptoService.Encrypt(login.Password);

            var employee = provider.Authenticate(login);

            if (employee == null) return null;

            var token = GenerateJwtToken(employee);

            return new AuthenticateResponse(employee, token);
        }

        #endregion

        #region Overrides

        public override Employee GetByID(long iD)
        {
            var entity = base.GetByID(iD);

            return entity;
        }

        public override Employee GetActiveByID(long iD)
        {
            var entity = base.GetActiveByID(iD);

            return entity;
        }

        public override IList<Employee> GetAll()
        {
            var entities = base.GetAll();

            return entities;
        }
        public override IList<Employee> GetAllActive()
        {
            var entities = base.GetAllActive();

            return entities;
        }

        #endregion

        #region Private Methods

        private string GenerateJwtToken(Employee employee)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Sid, employee.ID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        #endregion
    }
}

