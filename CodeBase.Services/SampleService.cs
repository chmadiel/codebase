/*
*/
#region using directives
using CodeBase.Common;
using CodeBase.DAL;
using CodeBase.Entities;
using System;
#endregion

namespace CodeBase.Services
{
    public class SampleService : SampleServiceBase, ISampleService
    {
        public SampleService(ISampleProvider provider) : base(provider) { }

        public override Sample Save(Sample entity, bool createTransaction)
        {
            try
            {
                return base.Save(entity, createTransaction);
            } 
            catch(Exception ex)
            {
                if(ex.Message.Contains("The DELETE statement conflicted with the REFERENCE constraint"))
                {
                    throw new AppException("Cannot delete this item. It is referenced in another table.");
                }
                throw ex;
            }
        }
    }
}

