﻿using CodeBase.Common;
using CodeBase.Entities;
using CodeBase.Entities.CacheObjects;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeBase.Services
{
    public class DataCacheHelper : IDataCacheHelper
    {
        #region Properties

        readonly ISampleService _sampleService;

        #endregion

        #region Constructors

        public DataCacheHelper(ISampleService sampleService)
        {
            _sampleService = sampleService;
        }

        #endregion

        #region GetAllMethods

        /// <summary>
        ///     only called if the cache is expired. and system needs to reload cache again.
        ///     recursive calls only runs twice
        ///     this technique is used for all the cached items.
        ///     it tries to get data from cache, if not found then loads from DB.
        /// </summary>
        /// <param name="dataCache"></param>
        /// <param name="callNo"></param>
        /// <returns></returns>
        public IList<SampleCache> GetAllZones(DataCache dataCache, int callNo)
        {
            if (callNo > 1) return new List<SampleCache>();

            var cacheItem = DataCache.Cache.Get(CacheConstants.ZoneCacheKey) as IList<SampleCache>;

            if (cacheItem == null)
            {
                CacheTerritoryData(dataCache);
                callNo++;
                return GetAllZones(dataCache, callNo);
            }

            return cacheItem;
        }

        #endregion

        #region CacheDataMethods

        /// <summary>
        ///     cache territory data.
        ///     for performance reasons, method avoids LINQ, therefore, lenght exceeds the LRMIS limit.
        ///     Due to complexity and dependency of each object onto parent object, this method is hard
        ///     to break into multiple methods.
        /// </summary>
        /// <param name="dataCache"></param>
        public void CacheTerritoryData(DataCache dataCache)
        {
            var policy = dataCache.GetTerritoryPolicy();

            var amples = _sampleService.GetAllActive();

            //var sampleDictionary = new Dictionary<long, IList<SiteCache>>();
            var cachedZones = new List<SampleCache>();
            foreach (var zone in amples)
            {
                var z = new SampleCache(zone.ID, zone.Samplename, zone.SampleAnotherid.Value);
                cachedZones.Add(z);
                //sampleDictionary.Add(z.SampleId, new List<SiteCache>());

                //if (regionDictionary.ContainsKey(z.SampleAnotherId))
                //{
                //    var list = regionDictionary[z.SampleAnotherId];
                //    list.Add(z);
                //}
            }

            DataCache.Cache.Set(CacheConstants.ZoneCacheKey, cachedZones, policy);
            //DataCache.Cache.Set(CacheConstants.RegionZoneCacheKey, regionDictionary, policy);

            //var sites = _siteService.GetAllActive();

            //var cachedSites = new List<SiteCache>();
            //foreach (var site in sites)
            //{
            //    var s = new SiteCache(site.ID, site.Sitename, site.Zoneid.Value, site.ZoneName, site.RegionId, site.RegionName, site.Sitetype.Value);
            //    cachedSites.Add(s);

            //    if (sampleDictionary.ContainsKey(s.ZoneId))
            //    {
            //        var list = sampleDictionary[s.ZoneId];
            //        list.Add(s);
            //    }
            //}

            //DataCache.Cache.Set(CacheConstants.SiteCacheKey, cachedSites, policy);
            //DataCache.Cache.Set(CacheConstants.ZoneSiteCacheKey, sampleDictionary, policy);
        }

        public void AddZoneToCache(DataCache dataCache, Sample sample)
        {
            var policy = dataCache.GetTerritoryPolicy();

            var cacheItem = DataCache.Cache.Get(CacheConstants.ZoneCacheKey) as IList<SampleCache>;
            if (cacheItem == null) CacheTerritoryData(dataCache);

            var item = cacheItem.Where(x => x.SampleId == sample.ID)?.FirstOrDefault();

            if (item != null)
            {
                item.SampleName = sample.Samplename;
                DataCache.Cache.Set(CacheConstants.ZoneCacheKey, cacheItem, policy);
                return;
            }

            cacheItem.Add(new SampleCache(sample.ID, sample.Samplename, sample.SampleAnotherid.Value));

            DataCache.Cache.Set(CacheConstants.ZoneCacheKey, cacheItem, policy);

            //var cacheDict =
            //    DataCache.Cache.Get(CacheConstants.ZoneSiteCacheKey) as Dictionary<long, List<SiteCache>>;

            //var sites = _siteService.GetListByForeignKeyZoneid(sample.ID);
            //var cachedSites = sites.Select(x => new SiteCache(x.ID, x.Sitename, sample.ID, x.ZoneName, x.RegionId, x.RegionName, x.Sitetype.Value)).ToList();
            //if (cacheDict.ContainsKey(sample.ID))
            //{
            //    cacheDict[sample.ID].AddRange(cachedSites);
            //}
            //else
            //{
            //    cacheDict.Add(sample.ID, cachedSites);
            //}

            //DataCache.Cache.Set(CacheConstants.ZoneSiteCacheKey, cacheDict, policy);

            var cacheRegionDict =
                DataCache.Cache.Get(CacheConstants.RegionZoneCacheKey) as Dictionary<int, IList<SampleCache>>;

            if (cacheRegionDict.ContainsKey(sample.SampleAnotherid.Value))
            {
                cacheRegionDict[sample.SampleAnotherid.Value].Add(new SampleCache(sample.ID, sample.Samplename, sample.SampleAnotherid.Value));
            }
            else
            {
                cacheRegionDict.Add(sample.SampleAnotherid.Value, new List<SampleCache>() { new SampleCache(sample.ID, sample.Samplename, sample.SampleAnotherid.Value) });
            }

            DataCache.Cache.Set(CacheConstants.RegionZoneCacheKey, cacheRegionDict, policy);
        }

        #endregion
    }
}
