/*
*/
#region using directives
using CodeBase.Entities;
using CodeBase.Entities.ViewModels;
using CodeBase.Services;
#endregion

namespace CodeBase.Services
{
    public interface IEmployeeService : IEmployeeServiceBase
    {
        AuthenticateResponse Authenticate(LoginDto login);
    }
}
