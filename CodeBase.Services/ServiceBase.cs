﻿using CodeBase.Common;
using CodeBase.Entities;
using System;

namespace CodeBase.Services
{
    public abstract class ServiceBase : IDisposable
    {
        public Employee ActiveUser { get; set; }
        public ServiceBase()
        {
            ActiveUser = null;
        }
        //todo AN: apply Dispose pattern. 
        public void Dispose()
        {
        }
    }
}
