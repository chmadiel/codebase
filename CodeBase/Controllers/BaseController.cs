﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CodeBase.Common;
using CodeBase.Entities;
using CodeBase.Filters;
using CodeBase.Services;
using System;
using System.Linq;
using System.Security.Claims;

namespace CodeBase.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    [AppActionFilter]
    public class BaseController<T> : ControllerBase where T : BaseController<T>
    {
        private Employee _activeUser;

        public Employee ActiveUser => _activeUser ??= HttpContext.RequestServices.GetService<IEmployeeService>().GetByID(Convert.ToInt64(HttpContext.User.Claims.Where(x => x.Type == ClaimTypes.Sid)?.FirstOrDefault()?.Value ?? "0"));

        private ILogger<T> _logger;

        protected ILogger<T> Logger => _logger ??= HttpContext.RequestServices.GetService<ILogger<T>>();
    }
}
