﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CodeBase.Common;
using CodeBase.Entities.ViewModels;
using CodeBase.Services;
using System;
using System.Diagnostics;

namespace CodeBase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        IEmployeeService _employeeService;
        ILogger<LoginController> _logger;
        IDataCache _dataCache;

        public LoginController(ILogger<LoginController> logger, IEmployeeService employeeService, IDataCache dataCache)
        {
            _employeeService = employeeService;
            _logger = logger;
            _dataCache = dataCache;
        }

        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] LoginDto login)
        {
            var response = new ApiResponse() { OperationStatus = false };
            try
            {
                response.AdditionalData = _employeeService.Authenticate(login);
                response.OperationStatus = true;
                if (response.AdditionalData == null)
                {
                    response.Message = "No Results Found";
                    response.OperationStatus = false;
                }
            }
            catch (AppException ex)
            {
                response.Message = ex.ToString();
                response.OperationStatus = false;
            }
            catch (Exception ex)
            {
                var stackTrace = new StackTrace(ex, true);
                var frame = stackTrace.GetFrame(0);
                string messageTemplate = "An Exception Occured at {0}.{1}; Line Number: {2}";
                var exMessage = string.Format(messageTemplate, ControllerContext.RouteData.Values["controller"].ToString(), ControllerContext.RouteData.Values["action"].ToString(), frame.GetFileLineNumber());
                var payload = JsonConvert.SerializeObject(login);
                exMessage += "\nPayload: {@payload}";
                _logger.LogError(ex, exMessage, payload);
                response.Message = "An Exception has occured. Please view DB Logs";
                response.OperationStatus = false;
            }
            return Ok(response);
        }

        [HttpGet("RefreshCache")]
        public IActionResult RefreshCache()
        {
            var response = new ApiResponse() { OperationStatus = false };
            try
            {
                _dataCache.RefreshCache();
                response.OperationStatus = true;
                response.Message = "Cache Refreshed Successfully";
            }
            catch (AppException ex)
            {
                response.OperationStatus = false;
                response.Message = ex.Message;
            }
            catch (Exception ex)
            {
                var stackTrace = new StackTrace(ex, true);
                var frame = stackTrace.GetFrame(0);
                _logger.LogError(ex, "An Exception Occured at " + ControllerContext.RouteData.Values["controller"].ToString() + "." + ControllerContext.RouteData.Values["action"].ToString() + "; Line Number: " + frame.GetFileLineNumber());
                response.Message = "An Exception has occured. Please view DB Logs";
            }
            return Ok(response);
        }

        [HttpPost("test")]
        public IActionResult Test([FromBody] LoginDto login)
        {
            var response = new ApiResponse() { OperationStatus = true };

            response.AdditionalData = login;
            response.Message = "Mission Successful";

            return Ok(response);
        }
    }
}
