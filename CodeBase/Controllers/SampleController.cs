﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CodeBase.Common;
using CodeBase.Entities;
using CodeBase.Entities.ViewModels;
using CodeBase.Services;
using System;
using System.Diagnostics;
using System.Linq;

namespace CodeBase.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SampleController : BaseController<SampleController>
    {
        ISampleService _entityService;
        public SampleController(ISampleService entityService)
        {
            _entityService = entityService;
        }

        [HttpPost]
        public IActionResult Save([FromBody] Sample entity)
        {
            var response = new ApiResponse() { OperationStatus = false };
            try
            {
                if (entity.ID == 0)
                {
                    entity.Createdby = ActiveUser.ID;
                    entity.Createddate = DateTime.UtcNow;
                    entity.EntityCreated();
                }
                else
                {
                    var oldEntity = _entityService.GetByID(entity.ID);
                    entity.Updatedby = ActiveUser.ID;
                    entity.Updateddate = DateTime.UtcNow;
                    entity.Createdby = oldEntity.Createdby;
                    entity.Createddate = oldEntity.Createddate;
                    entity.EntityModified();
                }
                var updatedEntity = _entityService.Save(entity, true);

                if (updatedEntity != null)
                {
                    response.AdditionalData = updatedEntity;
                    response.OperationStatus = true;
                }
            }
            catch (AppException ex)
            {
                response.Message = ex.Message;
                response.OperationStatus = false;
            }
            catch (Exception ex)
            {
                var stackTrace = new StackTrace(ex, true);
                var frame = stackTrace.GetFrame(0);
                string messageTemplate = "An Exception Occured at {0}.{1}; Line Number: {2}";
                var exMessage = string.Format(messageTemplate, ControllerContext.RouteData.Values["controller"].ToString(), ControllerContext.RouteData.Values["action"].ToString(), frame.GetFileLineNumber());
                exMessage += "\nPayload: {@payload}";
                var payload = JsonConvert.SerializeObject(entity);
                Logger.LogError(ex, exMessage, payload);
                response.Message = "An Exception has occured. Please view DB Logs";
            }
            return Ok(response);
        }

        [HttpGet]
        [HttpGet("{id}")]
        public IActionResult Get(int? id)
        {
            var response = new ApiResponse() { OperationStatus = false };
            try
            {
                if (id.HasValue)
                {
                    var entity = _entityService.GetByID(id.Value);
                    if (entity != null)
                        return Ok(entity);
                    else
                        return Ok("No Results Found");
                }
                else
                {
                    var entities = _entityService.GetAll();

                    if (entities != null && entities.Any())
                    {
                        response.AdditionalData = entities;
                        response.OperationStatus = true;
                    }
                    else if (entities != null)
                        throw new AppException("No Results Found");
                }
            }
            catch (AppException ex)
            {
                response.Message = ex.Message;
                response.OperationStatus = false;
            }
            catch (Exception ex)
            {
                var stackTrace = new StackTrace(ex, true);
                var frame = stackTrace.GetFrame(0);
                Logger.LogError(ex, "An Exception Occured at " + ControllerContext.RouteData.Values["controller"].ToString() + "." + ControllerContext.RouteData.Values["action"].ToString() + "; Line Number: " + frame.GetFileLineNumber());
                response.Message = "An Exception has occured. Please view DB Logs";
            }
            return Ok(response);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var response = new ApiResponse() { OperationStatus = false };
            try
            {
                var entity = new Sample() { ID = id };
                entity.EntityDeleted();
                entity = _entityService.Save(entity, true);

                if (entity != null)
                {
                    response.AdditionalData = entity;
                    response.OperationStatus = true;
                }
                else
                    throw new AppException("No Results Found");
            }
            catch (AppException ex)
            {
                response.Message = ex.Message;
                response.OperationStatus = false;
            }
            catch (Exception ex)
            {
                var stackTrace = new StackTrace(ex, true);
                var frame = stackTrace.GetFrame(0);
                Logger.LogError(ex, "An Exception Occured at " + ControllerContext.RouteData.Values["controller"].ToString() + "." + ControllerContext.RouteData.Values["action"].ToString() + "; Line Number: " + frame.GetFileLineNumber());
                response.Message = "An Exception has occured. Please view DB Logs";
            }
            return Ok(response);
        }
    }
}
