﻿using Microsoft.AspNetCore.Mvc;
using RMS.Filters;
using System.Linq;
using System.Security.Claims;

namespace RMS.Attributes
{
    public class AuthenticateAttribute : TypeFilterAttribute
    {
        public AuthenticateAttribute(params string[] permissions) : base(typeof(AuthenticateFilter))
        {
            Arguments = new object[] { permissions.Select(x => new Claim("Permission", x)) };
        }
    }
}
