﻿using Microsoft.AspNetCore.Mvc.Filters;
using CodeBase.Common;
using CodeBase.Entities;
using CodeBase.Services;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;

namespace CodeBase.Filters
{
    public class AppActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var employeeService = (IEmployeeService)context.HttpContext.RequestServices.GetService(typeof(IEmployeeService));
            var employeeId = Convert.ToInt64(context.HttpContext.User.Claims.Where(x => x.Type == ClaimTypes.Sid)?.FirstOrDefault()?.Value ?? "0");

            if (employeeId != 0) {
                Employee activeuser = null;
                if (activeuser == null)
                {
                    activeuser = employeeService.GetByID(employeeId);
                }
                else
                {
                    if(employeeId != activeuser.ID)
                    {
                        activeuser = employeeService.GetByID(employeeId);
                    }
                }
            }
            base.OnActionExecuting(context);
        }
    }
}
